using MSharp;

namespace Domain
{
    public class Status : EntityType
    {
        public Status()
        {
            GenerateParseMethod();
            IsEnumReference();

            InstanceAccessors(new string[] { "Pending", "Started", "Completed", "Rejected" });

            String("Title");
        }
    }
}