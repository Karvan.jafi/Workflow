using MSharp;

namespace Domain
{
    public class ActionRequest : EntityType
    {
        public ActionRequest()
        {
            Abstract();

            Date("Date requested").Default("c#:LocalTime.Now").Mandatory();

            ToStringExpression("DateRequested.ToShortDateString()");

            Associate<PeopleService.UserInfo>("Requested by").Calculated().Getter("Task.Factory.RunSync(() => new PeopleService.HubApi().Cache(CachePolicy.CacheOrFreshOrNull).GetUserInfo(RequestedByEmail))");

            Associate<PeopleService.UserInfo>("Assignee").Calculated().Getter("AssigneeEmail.HasValue() ? Task.Factory.RunSync(() => new PeopleService.HubApi().Cache(CachePolicy.CacheOrFreshOrNull).GetUserInfo(AssigneeEmail)) : null");

            String("RequestedByEmail").Mandatory().Documentation("Hold the unique email of the UserInfo calss in the PeopleService");
            String("AssigneeEmail").Documentation("Hold the unique email of the UserInfo calss in the PeopleService");

            Associate<Status>("Status").Default("Pending");
            Associate<Urgency>("Urgency");
        }
    }
}