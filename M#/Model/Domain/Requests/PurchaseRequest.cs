using MSharp;

namespace Domain
{
    public class PurchaseRequest : SubType<ActionRequest>
    {
        public PurchaseRequest()
        {
            String("Product url");

            Int("Quantity");

            Money("Unit cost");
        }
    }
}