using MSharp;

namespace Domain
{
    public class RequestType : EntityType
    {
        public RequestType()
        {
            String("Title").Mandatory();
            String("Url").Mandatory().HelpText("Relative URL of the Request, this URL should indicate to the New page. ex: request-type/purchase-request");
            String("Description").Max(2500);
        }
    }
}