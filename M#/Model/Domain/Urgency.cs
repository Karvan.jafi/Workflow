using MSharp;

namespace Domain
{
    public class Urgency : EntityType
    {
        public Urgency()
        {
            GenerateParseMethod();
            IsEnumReference();

            InstanceAccessors(new string[] { "Urgent", "Today", "This week", "Any time" });

            String("Title");
        }
    }
}