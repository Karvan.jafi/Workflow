using MSharp;

public class MyRequestPage : RootPage
{
    public MyRequestPage()
    {
        Route("/my-request");

        Add<Modules.MyRequestsList>();
    }
}