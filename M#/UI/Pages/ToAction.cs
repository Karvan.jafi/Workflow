using MSharp;

public class ToActionPage : RootPage
{
    public ToActionPage()
    {
        Route("/to-action");

        Add<Modules.OpenRequestsList>();
    }
}