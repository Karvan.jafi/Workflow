﻿using MSharp;

namespace ActionRequest
{
    public class PurchaseRequestPage : SubPage<RequestTypePage>
    {
        public PurchaseRequestPage()
        {
            Layout(Layouts.Modal);
            Add<Modules.PurchaseRequestForm>();
        }
    }
}
