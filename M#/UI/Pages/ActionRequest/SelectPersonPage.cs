﻿using MSharp;

namespace ActionRequest
{
    public class SelectPersonPage : SubPage<ToActionPage>
    {
        public SelectPersonPage()
        {
            Layout(Layouts.Modal);
            Add<Modules.SelectPerson>();
        }
    }
}
