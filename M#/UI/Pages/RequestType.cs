using MSharp;

public class RequestTypePage : RootPage
{
    public RequestTypePage()
    {
        Route("/request-type");

        Add<Modules.RequestTypesList>();
    }
}