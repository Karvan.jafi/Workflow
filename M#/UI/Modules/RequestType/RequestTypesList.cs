using MSharp;

namespace Modules
{
    public class RequestTypesList : ListModule<Domain.RequestType>
    {
        public RequestTypesList()
        {
            HeaderText("Select a request type");

            Column(x => x.Title).HeaderTemplate("Request type");
            Column(x => x.Description);

            ButtonColumn("Select")
                //.ExtraTagAttributes("data-redirect=\"ajax\"")
                .OnClick(x => x.PopUp("c#:item.Url"));

            //ButtonColumn("Select").HeaderText("select").GridColumnCssClass("actions").Icon(FA.Edit)
            //    .OnClick(x => x.Go <...Page > ().Send("item", "item.ID"));

            Button("Back").Icon(FA.Backward)
                .OnClick(x => x.ReturnToPreviousPage());
        }
    }
}