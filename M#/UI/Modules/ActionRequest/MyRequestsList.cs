using MSharp;

namespace Modules
{
    public class MyRequestsList : ListModule<Domain.ActionRequest>
    {
        public MyRequestsList()
        {
            HeaderText("My requests");

            Column(x => x.DateRequested);
            Column(x => x.Assignee);
            Column(x => x.Status);
            Column(x => x.Urgency);

            DataSource("await Database.GetList<ActionRequest>().Where(x => x.Assignee == GetCurrentUser())");

            ButtonColumn("Cancel").Icon(FA.Remove)
                .OnClick(x =>
                {
                    x.CSharp("await item.CancelRequest();");
                    x.RefreshPage();
                });

            Button("New request").Icon(FA.Plus)
                .OnClick(x => x.Go<RequestTypePage>().SendReturnUrl());
        }
    }
}