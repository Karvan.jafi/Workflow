using MSharp;

namespace Modules
{
    public class OpenRequestsList : ListModule<Domain.ActionRequest>
    {
        public OpenRequestsList()
        {
            HeaderText("Open requests");

            Column(x => x.DateRequested);
            Column(x => x.RequestedBy);
            Column(x => x.Status);
            Column(x => x.Urgency);
            Column(x => x.Assignee).EmptyMarkup("Unassigned");

            CustomColumn().HeaderTemplate("Assign").DisplayExpression("@if (item.Assignee.ToStringOrEmpty().IsEmpty()) { <a href=\"to-action/select-person/@item.ID\" target=\"$modal\">select assignee...</a> <span>| Suggestion: (soon)</span> } else { <a href=\"to-action/select-person/@item.ID\" target=\"$modal\">reassign to...</a> }");

            Button("New request").Icon(FA.Plus)
                .OnClick(x => x.Go<RequestTypePage>().SendReturnUrl());
        }
    }
}