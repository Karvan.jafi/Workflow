using MSharp;

namespace Modules
{
    public class SelectPerson : FormModule<Domain.ActionRequest>
    {
        public SelectPerson()
        {
            HeaderText("Select person form");

            WrapInForm(false).SupportsAdd(false).SupportsEdit(true);

            MarkupWrapper(" <select id=\"personSelect\"> <option>Select a person</option> </select> <input id=\"personId\" type=\"hidden\" value=\"@Model.Item.ID\" />");

            OnBound("JS dependency").Code("var module = JavascriptModule.Absolute(\"scripts/personModule.js?v=1\"); JavaScript(module);");
        }
    }
}