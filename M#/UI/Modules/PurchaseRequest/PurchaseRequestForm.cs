using MSharp;

namespace Modules
{
    public class PurchaseRequestForm : FormModule<Domain.PurchaseRequest>
    {
        public PurchaseRequestForm()
        {
            HeaderText("Purchase request");

            Field(x => x.RequestedBy).Readonly();
            Field(x => x.DateRequested).Readonly();

            Field(x => x.Urgency).Control(ControlType.HorizontalRadioButtons);

            Field(x => x.ProductUrl);
            Field(x => x.Quantity);
            Field(x => x.UnitCost);

            OnBound("Set current user email").Code("info.Item.RequestedByEmail = User.GetEmail();");

            Button("Cancel").OnClick(x => x.CloseModal());

            Button("Save").IsDefault().Icon(FA.Check)
            .OnClick(x =>
            {
                x.SaveInDatabase();
                x.GentleMessage("Saved successfully.");
                x.CloseModal(Refresh.None);
                x.CSharp("await Database.Refresh(); //TODO: this should be removed");
            });
        }
    }
}