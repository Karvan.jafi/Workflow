﻿
import OlivePage from "olive/olivePage"
import Waiting from "olive/components/waiting";
import Url from "olive/components/url";
import Select from "olive/plugins/select";
import Alert from "olive/components/alert";

declare var requirejs: any;

export default class PersonModule {

    static get page(): OlivePage { return window["page"]; }

    public static run(): void {

        requirejs(["app/model/service"], (Service) => { this.getData(Service) });

        $("#personSelect").chosen().change((val, item) => {

            let personId = $("#personId").val();

            var url: string = Url.effectiveUrlProvider("", null);

            $.ajax({
                url: url + "api/changeAssignee/" + personId + "/" + item.selected,
                type: 'GET',
                xhrFields: { withCredentials: true },
                success: (response: any) => {
                    Alert.alertUnobtrusively("Assignee changed successfully");
                },
                error: (response) => {
                    console.log(response);
                },
                complete: (response) => {
                    Waiting.hide();
                }
            });

        });
    }

    private static getData(Service) {

        var baseUrl = Service.default.fromName("people").BaseUrl;

        Waiting.show();

        $.ajax({
            url: baseUrl + "/api/EmployeeProfiles/all",
            type: 'GET',
            xhrFields: { withCredentials: true },
            success: (response: any) => {

                $.each(response, (i, val) => {
                    $("#personSelect").append(`<option value="${val.Email}">${val.Name}</option>`);

                    $("#personSelect").trigger("chosen:updated");
                });
            },
            error: (response) => {
                console.log(response);
            },
            complete: (response) => {
                Waiting.hide();
                $("#myModal").on('hidden.bs.modal', () => {
                    this.page.refresh();
                });
            }
        });
    }
}