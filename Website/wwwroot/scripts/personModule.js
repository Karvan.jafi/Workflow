define(["require", "exports", "olive/components/waiting", "olive/components/url", "olive/components/alert"], function (require, exports, waiting_1, url_1, alert_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    var PersonModule = /** @class */ (function () {
        function PersonModule() {
        }
        Object.defineProperty(PersonModule, "page", {
            get: function () { return window["page"]; },
            enumerable: true,
            configurable: true
        });
        PersonModule.run = function () {
            var _this = this;
            requirejs(["app/model/service"], function (Service) { _this.getData(Service); });
            $("#personSelect").chosen().change(function (val, item) {
                var personId = $("#personId").val();
                var url = url_1.default.effectiveUrlProvider("", null);
                $.ajax({
                    url: url + "api/changeAssignee/" + personId + "/" + item.selected,
                    type: 'GET',
                    xhrFields: { withCredentials: true },
                    success: function (response) {
                        alert_1.default.alertUnobtrusively("Assignee changed successfully");
                    },
                    error: function (response) {
                        console.log(response);
                    },
                    complete: function (response) {
                        waiting_1.default.hide();
                    }
                });
            });
        };
        PersonModule.getData = function (Service) {
            var _this = this;
            var baseUrl = Service.default.fromName("people").BaseUrl;
            waiting_1.default.show();
            $.ajax({
                url: baseUrl + "/api/EmployeeProfiles/all",
                type: 'GET',
                xhrFields: { withCredentials: true },
                success: function (response) {
                    $.each(response, function (i, val) {
                        $("#personSelect").append("<option value=\"" + val.Email + "\">" + val.Name + "</option>");
                        $("#personSelect").trigger("chosen:updated");
                    });
                },
                error: function (response) {
                    console.log(response);
                },
                complete: function (response) {
                    waiting_1.default.hide();
                    $("#myModal").on('hidden.bs.modal', function () {
                        _this.page.refresh();
                    });
                }
            });
        };
        return PersonModule;
    }());
    exports.default = PersonModule;
});
//# sourceMappingURL=personModule.js.map