namespace MS.WorkflowService
{
    using System;
    using Domain;
    using System.Linq;
    using System.ComponentModel;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Authorization;
    using Olive.Entities;
    using Olive.Mvc;
    using Microsoft.AspNetCore.Http;
    using Olive;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Controllers;

    /* NOTE ===================================
    *  See this before creating your API:
    *  https://github.com/Geeksltd/Olive/blob/master/Integration/Olive.Microservices/Integration.md
    *  
    *  To see the generated Api documentation, request → http://(rootUrl)/swagger
    *  ======================================== */

    [Route("api")]
    public class WorkflowApi : BaseController
    {

        [HttpGet, Route("changeAssignee/{id:Guid}/{email}")]
        // [AuthorizeTrustedService]  or  [AuthorizeService("ServiceName")]  or  [Authorize(Roles = "...")]
        public async Task<IActionResult> ChangeAssignee(Guid id, string email)
        {
            var request = await Database.Get<ActionRequest>(id);

            await Database.Update(request, (s) => s.AssigneeEmail = email);

            return Ok();
        }
    }
}