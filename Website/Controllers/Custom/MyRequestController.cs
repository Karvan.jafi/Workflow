﻿using Olive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PeopleService;

namespace Controllers
{
    public partial class MyRequestController
    {
        public UserInfo GetCurrentUser()
        {
            return Task.Factory.RunSync(() => new HubApi().Cache(CachePolicy.CacheOrFreshOrNull).GetUserInfo(User.GetEmail()));
        }
    }
}
