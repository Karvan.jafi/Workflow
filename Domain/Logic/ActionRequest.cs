﻿using System.Threading.Tasks;

namespace Domain
{
    public abstract partial class ActionRequest
    {
        public abstract void Fulfil();
        public abstract void SelectAssignees();

        public async Task CancelRequest()
        {
            if (Status == Status.Pending || Status == Status.Started)
            {
                await Database.Update(this, (a) =>
                {
                    a.Status = Status.Pending;
                    a.AssigneeEmail = null;
                });

                await Database.Refresh(); //TODO: this should be removed 
            }
        }
    }
}
