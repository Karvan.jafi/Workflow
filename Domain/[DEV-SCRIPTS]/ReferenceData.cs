﻿using System;
using System.Threading.Tasks;
using Olive;
using Olive.Entities;

namespace Domain
{
    public class ReferenceData
    {
        static async Task<T> Create<T>(T item) where T : IEntity
        {
            await Context.Current.Database().Save(item, SaveBehaviour.BypassAll);
            return item;
        }

        public static Task Create()
        {
            return InitData();
        }

        static async Task InitData()
        {
            //init Urgency
            var urgent = new Urgency()
            {
                ID = Guid.Parse("F1FFE0F1-A548-43B3-AAFC-82AFAB5F991C"),
                Title = "Urgent"
            };

            await Create(urgent);

            urgent = new Urgency()
            {
                ID = Guid.Parse("DDD57CE2-F486-4E7A-B74B-A2F2C595C9A4"),
                Title = "Today"
            };

            await Create(urgent);

            urgent = new Urgency()
            {
                ID = Guid.Parse("9E6148AD-7533-49AC-B08E-D09B57B29737"),
                Title = "This week"
            };

            await Create(urgent);

            urgent = new Urgency()
            {
                ID = Guid.Parse("E45342FA-A026-4FF3-83F1-D77A71D9A8E0"),
                Title = "Any time"
            };

            await Create(urgent);

            //init Status
            var status = new Status()
            {
                ID = Guid.Parse("0F80BDC2-54AA-4C79-BDA1-B83424EE83C5"),
                Title = "Pending"
            };

            await Create(status);

            status = new Status()
            {
                ID = Guid.Parse("E3D0DB71-3BAD-4CC8-BCD0-516812F42E2C"),
                Title = "Started"
            };

            await Create(status);

            status = new Status()
            {
                ID = Guid.Parse("7435ED91-DE0F-457B-8772-29439727A492"),
                Title = "Completed"
            };

            await Create(status);

            status = new Status()
            {
                ID = Guid.Parse("35D533A4-7FE8-49BC-9321-15E1D50C18DC"),
                Title = "Rejected"
            };

            await Create(status);

            //init Request type
            var requestType = new RequestType()
            {
                ID = Guid.Parse("2377C455-56E3-4D08-AC97-31DC176BA4A8"),
                Title = "Purchase request",
                Description = "Purchase workflow",
                Url = "request-type/purchase-request"
            };

            await Create(requestType);
        }
    }
}